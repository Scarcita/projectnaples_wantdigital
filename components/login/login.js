import React, { useState } from "react";
import { View,Text, StyleSheet, Image, TouchableOpacity, SafeAreaView, FlatList, TextInput } from "react-native";
import { useNavigation } from "@react-navigation/native";

////IMAGENES//////


const Login = () => {
  const navigation = useNavigation();
  const [userName, onChangeUserName] = useState("");
  const [password, onChangePassword] = useState("");


  return ( 
        <View style={styles.container}>
            <View style={styles.boxWellcome}>
                <Text style={styles.textHello}> 
                    Hello Again!
                </Text>
                <Text style={styles.message}>
                     back you've been missed!
                </Text>
            </View>
            <View style={styles.boxInput}>
                <TextInput 
                    style={styles.inputUsername}
                    placeholder='Enter Username'
                    onChangeText={onChangeUserName}
                    value={userName}
                    keyboardType="string"
                />
                <TextInput 
                    style={styles.inputPassword}
                    placeholder='password'
                    onChangeText={onChangePassword}
                    value={password}
                    //keyboardType="string"
                />
          </View>
          <View style={styles.boxButton}>
            <TouchableOpacity
                onPress={() => (navigation.navigate('CreditCard'))}
                style={styles.button}
            >
                <Text
                    style={styles.textButton}
                >
                    Login
                </Text>
            </TouchableOpacity>
          </View>
          <View style={{flexDirection: 'row', justifyContent: 'center', height: 80,justifyContent:'center', alignItems:'center'}}>
            <Text style={styles.interrogantText}>Already a member?</Text>
            <TouchableOpacity
                onPress={() => (navigation.navigate('RegisterOne'))}
            >
                <Text style={styles.signInText}>
                    Sign In
                </Text>
            </TouchableOpacity>
            
          </View>
          
        </View>
      
    
  )
    
}

export default Login;

const styles = StyleSheet.create ({
  container: {
    flex: 1,
    paddingTop: 10,
    paddingLeft: 35,
    paddingRight:35,
    backgroundColor: '#fff'  
  },
  boxWellcome: {
    marginTop: 30,
    height: 180,
    //backgroundColor: 'red',
    marginLeft: 30,
    marginRight: 30,
    justifyContent: 'center',
    alignItems: 'center'
  },
  textHello: {
    color: '#00B5C3',
    fontFamily: 'JosefinSans_700Bold',
    fontSize: 38,
    marginLeft: 10,
    alignSelf: 'center',
    alignItems: 'center',
    textAlign: 'center',
  },
  message:{
    fontSize: 24,
    color: '#747474',
    alignSelf: 'center', 
    alignItems: 'center',
    textAlign: 'center',
    fontFamily: 'JosefinSans_500Medium',
  },
  boxInput:{
    height: 240,
    //backgroundColor: 'blue',
    justifyContent: 'center',
    alignItems: 'center'

  },
  inputUsername: {
    backgroundColor: '#F5F5F5',
    width: 324,
    height: 42,
    borderRadius: 10,
    alignSelf: 'center',
    paddingLeft:15,
    fontFamily: 'JosefinSans_400Regular'
  },
  inputPassword: {
    backgroundColor: '#F5F5F5',
    width: 324,
    height: 42,
    borderRadius: 10,
    marginTop: 15,
    alignSelf: 'center',
    paddingLeft:15,
    fontFamily: 'JosefinSans_400Regular'
  },
  boxButton:{
    height: 200,
    //backgroundColor: 'red',
    justifyContent: 'center',
    alignItems: 'center'

  },
  button: {
    backgroundColor: "#00B5C3",
    width: 324,
    height: 58,
    alignSelf: 'center',
    borderRadius: 17,
    justifyContent:'center',
    alignItems:'center',

  },
  textButton: {
    textAlign: 'center',
    fontSize: 24,
    color: '#FFFFFF',
    fontFamily: 'JosefinSans_400Regular'
  },
  interrogantText: {
    color: '#00B5C3',
    fontFamily: 'JosefinSans_400Regular',
    fontSize: 15
  },
  signInText: {
    color: '#00B5C3',
    fontFamily: 'JosefinSans_700Bold',
    fontSize: 15
  }
 
});
