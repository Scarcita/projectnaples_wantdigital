import React from "react";
import { View,Text, StyleSheet, Image, TouchableOpacity, SafeAreaView, FlatList } from "react-native";
import { useNavigation } from "@react-navigation/native";

//// IMAGENES////////
import ImgPortada from '../../assets/Img/women.svg'

const RegisterOne = () => {
  const navigation = useNavigation();

  return ( 
        <View style={styles.container}>
          <ImgPortada
              style={styles.imgWomen}
            />

          <Text style={styles.title}>
           Help to shape the business climate in your local area
          </Text>

          <Text style={styles.description}>
          Find all our events to register easily more opportunities to make real connections
          </Text>

          <TouchableOpacity
            onPress={() => (navigation.navigate('two'))}
              style={styles.button}
          >
            <Text
                style={styles.textButton}
            >
                Continue
            </Text>
          </TouchableOpacity>
          <View style={{flexDirection: 'row', justifyContent: 'center', marginTop: 20}}>
            <Text style={styles.interrogantText}>Already a member?</Text>
            <Text style={styles.signInText}>Sign In</Text>
          </View>
        </View>
  )
    
}

export default RegisterOne;

const styles = StyleSheet.create ({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    
    
  },
  imgWomen: {
    textAlign: "center",
    alignSelf: 'center',
    width: '100%',
    height: '100%',
  },
  title: {
    color: '#000000',
    fontWeight: '700',
    fontSize: 35,
    marginTop: 10,
    textAlign: "center"
  },

  description: {
    color: '#000000',
    //fontWeight: 'bold',
    fontSize: 14,
    marginTop: 20,
    textAlign: "center"
  },

  button: {
    backgroundColor: "#FDB827",
    marginTop: 55,
    width: 320,
    height: 58,
    alignSelf: 'center',
    borderRadius: 17,
    justifyContent:'center',
    alignItems:'center',

  },
  textButton: {
    textAlign: 'center',
    fontSize: 24,
    color: '#FFFFFF',
  },
  interrogantText: {
    color: '#00B5C3',
    fontWeight: '400',
    fontSize: 15
  },
  signInText: {
    color: '#00B5C3',
    fontWeight: '700',
    fontSize: 15
  }

});
