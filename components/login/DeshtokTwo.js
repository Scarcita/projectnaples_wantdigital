import React from "react";
import { View,Text, StyleSheet, Image, TouchableOpacity, SafeAreaView, FlatList } from "react-native";
import { useNavigation } from "@react-navigation/native";

//// IMAGENES /////

import ImgPortada from '../../assets/Img/people.svg'
const Registertwo = () => {
  const navigation = useNavigation();


  return ( 
        <View style={styles.container}>
          <ImgPortada
              style={styles.imgPeople}
            />

          <Text style={styles.title}>
            Likeminded business owners that help you grow
          </Text>

          <Text style={styles.description}>
          Get in touch with people with your same aspirations and willing to make a change in Naples
          </Text>

          <TouchableOpacity
            onPress={() => (navigation.navigate('Three'))}
              style={styles.button}
          >
            <Text
                style={styles.textButton}
            >
                Continue
            </Text>
          </TouchableOpacity>
          <View style={{flexDirection: 'row', justifyContent: 'center', marginTop: 20}}>
            <Text style={styles.interrogantText}>Already a member?</Text>
            <Text style={styles.signInText}>Sign In</Text>
          </View>
        </View>
      
    
  )
    
}

export default Registertwo;

const styles = StyleSheet.create ({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    
    
  },
  imgPeople: {
    textAlign: "center",
    alignSelf: 'center',
    width: '100%',
    height: '100%'
  },
  title: {
    color: '#000000',
    fontWeight: 'bold',
    fontSize: 35,
    marginTop: 40,
    textAlign: "center"
  },

  description: {
    color: '#000000',
    //fontWeight: 'bold',
    fontSize: 14,
    marginTop: 15,
    textAlign: "center"
  },
  button: {
    backgroundColor: "#FDB827",
    marginTop: 55,
    width: 320,
    height: 58,
    alignSelf: 'center',
    borderRadius: 17,
    justifyContent:'center',
    alignItems:'center',

  },
  textButton: {
    textAlign: 'center',
    fontSize: 24,
    color: '#FFFFFF',

  },
  interrogantText: {
    color: '#00B5C3',
    fontWeight: '400',
    fontSize: 15
  },
  signInText: {
    color: '#00B5C3',
    fontWeight: '700',
    fontSize: 15
  }

});
