import React, { useState } from "react";
import { View,Text, StyleSheet, Image, TouchableOpacity, SafeAreaView, FlatList, TextInput } from "react-native";
import { LinearGradient } from 'expo-linear-gradient';
import DateTimePicker from '@react-native-community/datetimepicker';

////IMAGENES//////


const CreditCard = ({navigation}) => {
  
  const [numberCard, onChangeNumberCard] = useState("");
  const [cvv, onChangeCvv] = useState("");
  const [name, onChangeName] = useState("");

  const [datePicker, setDatePicker] = useState(false);
 
  const [date, setDate] = useState(new Date());
 
 
  function showDatePicker() {
    setDatePicker(true);
  };
 
 
  function onDateSelected(event, value) {
    setDate(value);
    setDatePicker(false);
  };


  return ( 
          <View style={styles.container}>
            <View style={styles.boxTitle}>
                <Text style={styles.title}>Credit Card</Text>
            </View>
            <View style={styles.boxCard}>
            <LinearGradient
              colors={['#00B5C3', '#077477']}
              style={styles.card}>
                <View style={{height: 100, justifyContent: 'center'}}>
                    <Text style={{color: '#fff', fontSize: 20, fontFamily: 'JosefinSans_400Regular'}}>{numberCard}</Text>
                    <Text style={{color: '#FDB827', fontSize: 16, fontFamily: 'JosefinSans_400Regular'}}>{cvv}</Text>
                </View>
                <View style={[{
                    flexDirection: "row", justifyContent: 'space-between', height: 100,}]}>
                    <View style={{flex: 1, justifyContent: 'center'}}>
                        <Text style={{color: '#fff', fontSize: 20, fontFamily: 'JosefinSans_400Regular'}}>{name}</Text>
                    </View>
                    <View style={{flex: 1, justifyContent: 'center'}}>
                        <Text style={{color: '#fff', fontSize: 20, fontFamily: 'JosefinSans_400Regular'}}>{date.toDateString()}</Text>
                    </View>
                </View>
                
            </LinearGradient>

            </View>
            <View style={styles.boxInput}>
              <View>
                  <Text style={styles.labelCardNumber}>
                      CARD NUMBER
                  </Text>
                  <TextInput 
                      style={styles.inputCardNumber}
                      placeholder='7572638392177'
                      onChangeText={onChangeNumberCard}
                      value={numberCard}
                      keyboardType="numeric"
                  />
              </View>
              <View style={[{
                  flexDirection: "row", justifyContent: 'space-between', marginTop:10
                }]}>
                  <View style={{flex: 1, marginRight: 10}}>
                      <Text style={styles.labelCardNumber}>
                          CVV
                      </Text>
                      <TextInput 
                          style={styles.inputCvv}
                          placeholder='028'
                          onChangeText={onChangeCvv}
                          value={cvv}
                          keyboardType="numeric"
                      />
                  </View>
                  <View style={{flex: 1, marginLeft: 10}}>
                      <Text style={styles.labelCardNumber}>
                          EXPERY DATE
                      </Text>
                      <View>
                        {!datePicker && (
                          <Text onPress={showDatePicker} style={styles.inputexpery}>{date.toDateString()}</Text>

                          )}
                          {datePicker && (
                            <DateTimePicker
                              value={date}
                              mode={'date'}
                              display={Platform.OS === 'ios' ? 'spinner' : 'default'}
                              is24Hour={true}
                              onChange={onDateSelected}
                              style={{justifyContent: 'center', alignItems: 'center'}}
                            />
                          )}
                      </View>
                  </View>
              </View>
              <View style={{marginTop:10}}>
                  <Text style={styles.labelCardNumber}>
                      CARD HOLDER
                  </Text>
                  <TextInput 
                      style={styles.inputCardNumber}
                      placeholder='Thomas Doe'
                      onChangeText={onChangeName}
                      value={name}
                      keyboardType="string"
                  />
              </View>
            </View>
            <View style={styles.boxButton}>
              <TouchableOpacity
                  onPress={() => (navigation.navigate('two'))}
                  style={styles.button}
              >
                  <Text
                      style={styles.textButton}
                  >
                      Continue
                  </Text>
              </TouchableOpacity>
            </View>
          
          </View>
      
    
  )
    
}

export default CreditCard;

const styles = StyleSheet.create ({
  container: {
    flex: 1,
    paddingTop: 10,
    paddingLeft: 35,
    paddingRight:35,
    backgroundColor: '#fff'  
  },
  boxTitle:{
    height: 40,
    //backgroundColor: 'red',
    justifyContent: 'center',
  },
  title:{
    color: '#000000',
    fontSize: 17,
    fontFamily: 'JosefinSans_700Bold',
  },
  boxCard:{
    height: 230,
    //backgroundColor: 'blue',
    justifyContent: 'center',
  },
  card: {
    width: '100%',
    height: 200,
    borderRadius:17,
    padding: 10
  },
  boxInput:{
    height: 280,
    //backgroundColor: 'green',
    justifyContent: 'center',
    alignItems: 'center'

  },
  labelCardNumber:{
    fontSize: 12,
    color: '#747474',
    fontFamily: 'JosefinSans_400Regular'

  },
  inputCardNumber: {
    backgroundColor: '#F5F5F5',
    width: 324,
    height: 42,
    borderRadius: 10,
    alignSelf: 'center',
    paddingLeft:15,
    marginTop: 5,
    color: '#747474',
    fontSize:14,
    fontFamily: 'JosefinSans_400Regular'
  },
  inputCvv: {
    backgroundColor: '#F5F5F5',
    width: '100%',
    height: 42,
    borderRadius: 10,
    alignSelf: 'center',
    paddingLeft:15,
    marginTop: 5,
    color: '#747474',
    fontSize:14,
    fontFamily: 'JosefinSans_400Regular'
  },
  inputexpery: {
    backgroundColor: '#F5F5F5',
    width: '100%',
    height: 42,
    borderRadius: 10,
    alignSelf: 'center',
    paddingLeft:15,
    paddingTop: 12,
    marginTop: 5,
    fontSize: 13,
    color: '#747474',
    fontFamily: 'JosefinSans_400Regular'
  },

  boxButton:{
    height: 190,
    //backgroundColor: 'red',
    justifyContent: 'center',
    alignItems: 'center'

  },
  button: {
    backgroundColor: "#00B5C3",
    width: 324,
    height: 58,
    alignSelf: 'center',
    borderRadius: 17,
    justifyContent:'center',
    alignItems:'center',

  },
  textButton: {
    textAlign: 'center',
    fontSize: 24,
    color: '#FFFFFF',
    fontFamily: 'JosefinSans_400Regular'
  },

 
});
