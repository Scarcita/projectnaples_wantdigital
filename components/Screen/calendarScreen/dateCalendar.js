import React, {useState, useEffect} from "react";
import { View,Text, StyleSheet, Image, TouchableOpacity, SafeAreaView, FlatList, TextInput } from "react-native";
import {
  useFonts,
  JosefinSans_100Thin,
  JosefinSans_200ExtraLight,
  JosefinSans_300Light,
  JosefinSans_400Regular,
  JosefinSans_500Medium,
  JosefinSans_600SemiBold,
  JosefinSans_700Bold,
  JosefinSans_100Thin_Italic,
  JosefinSans_200ExtraLight_Italic,
  JosefinSans_300Light_Italic,
  JosefinSans_400Regular_Italic,
  JosefinSans_500Medium_Italic,
  JosefinSans_600SemiBold_Italic,
  JosefinSans_700Bold_Italic,
} from '@expo-google-fonts/josefin-sans';

const DateCalendar = ({navigation}) => {
  let [fontsLoaded] = useFonts({
    JosefinSans_100Thin,
    JosefinSans_200ExtraLight,
    JosefinSans_300Light,
    JosefinSans_400Regular,
    JosefinSans_500Medium,
    JosefinSans_600SemiBold,
    JosefinSans_700Bold,
    JosefinSans_100Thin_Italic,
    JosefinSans_200ExtraLight_Italic,
    JosefinSans_300Light_Italic,
    JosefinSans_400Regular_Italic,
    JosefinSans_500Medium_Italic,
    JosefinSans_600SemiBold_Italic,
    JosefinSans_700Bold_Italic,
    });

  const [isLoading, setLoading] = useState(true);
  const [data, setData]= useState([]);

  const getUsuarios = async () => {
    try{
      const response = await
      fetch("https://reqres.in/api/users?page=2");
      const json = await response.json();
      console.log(json);
      setData(json.data);
    } catch (error){
      console.error(error);
    } finally {
      setLoading(false);
    }
  }

  useEffect(() => {
    getUsuarios();
  }, []);


  return ( 
    <FlatList
      horizontal = {true}
      data = {data}
      keyExtractor= {({id}) => id}
      renderItem={({item})=> (
        <View  style={[styles.flex, {
            flexDirection: "row", marginTop: 20,  marginBottom: 5
          }]}>
    
        <View style={{ flex: 1, height: 80, marginRight: 10,}} >
          <View style={styles.backgroundDate}>
            <View style={styles.dateAll}>
              <Text style={styles.nameDay}>Mon</Text>
              <Text style={styles.dayDate}>17</Text>
              <Text style={styles.nameMonth}>Dec</Text>
            </View>
          </View> 
        </View>
        <View style={{ flex: 1, height: 80, marginRight: 10}} >
          <View style={styles.backgroundDate}>
            <View style={styles.dateAll}>
              <Text style={styles.nameDay}>Mon</Text>
              <Text style={styles.dayDate}>18</Text>
              <Text style={styles.nameMonth}>Dec</Text>
            </View>
          </View>  
        </View>
        <View style={{ flex: 1, height: 80, marginRight: 10}} >
          <View style={styles.backgroundToday}>
            <View style={styles.dateAll}>
              <Text style={styles.nameDayToday}>Mon</Text>
              <Text style={styles.dayDateToday}>19</Text>
              <Text style={styles.nameMonthToday}>Dec</Text>
            </View>
          </View>  
        </View>
        <View style={{ flex: 1, height: 80, marginRight: 10}} >
          <View style={styles.backgroundDate}>
            <View style={styles.dateAll}>
              <Text style={styles.nameDay}>Mon</Text>
              <Text style={styles.dayDate}>20</Text>
              <Text style={styles.nameMonth}>Dec</Text>
            </View>
          </View> 
        </View>
        <View style={{ flex: 1, height: 80, marginRight: 10}} >
          <View style={styles.backgroundDate}>
            <View style={styles.dateAll}>
              <Text style={styles.nameDay}>Mon</Text>
              <Text style={styles.dayDate}>21</Text>
              <Text style={styles.nameMonth}>Dec</Text>
            </View>
          </View> 
        </View>
      </View>
      )}></FlatList>

  )
}

export default DateCalendar;

const styles = StyleSheet.create ({
  container: {
    flex: 1,
    paddingTop: 30,
    paddingLeft: 35,
    paddingRight:35,
    //backgroundColor: 'red'
  },
  title:{
    color: '#000000',
    fontSize: 17,
    fontFamily: 'JosefinSans_700Bold',
    paddingTop: 30
  },
  input: {
    backgroundColor: '#fff',
    width: 324,
    height: 42,
    borderRadius: 17,
    alignSelf: 'center',
    marginTop: 15,
    paddingLeft:15
  },
  flex: {
    flex: 1,
  },
  backgroundDate:{
    width: 60,
    height:72,
    backgroundColor: '#fff',
    borderRadius: 9,
    justifyContent:'center',
    alignItems:'center',
    shadowColor: "#000",
      shadowOffset: {
        width: 0,
        height: 4,
      },
      shadowOpacity: 0.37,
      shadowRadius: 17,

      elevation: 6,
  },
  dateAll:{
    justifyContent:'center',
    alignItems:'center',
  },
  nameDay: {
    color: '#FF0000',
    fontFamily: 'JosefinSans_400Regular',
    fontSize: 12,
  },
  dayDate: {
    color: '#000',
    fontFamily: 'JosefinSans_700Bold',
    fontSize: 24,
  },
  nameMonth: {
    color: '#000',
    fontFamily: 'JosefinSans_400Regular',
    fontSize: 12,
  },
  backgroundToday:{
    width: 60,
    height:72,
    backgroundColor: '#00B5C3',
    borderRadius: 9,
    alignSelf: 'center',
    shadowColor: "#000",
      shadowOffset: {
        width: 0,
        height: 4,
      },
      shadowOpacity: 0.37,
      shadowRadius: 17,

      elevation: 6,
  },
  dateAll:{
    textAlign: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    padding: 6
  },
  nameDayToday: {
    color: '#fff',
    fontFamily: 'JosefinSans_400Regular',
    fontSize: 12,
  },
  dayDateToday: {
    color: '#fff',
    fontFamily: 'JosefinSans_700Bold',
    fontSize: 24,
  },
  nameMonthToday: {
    color: '#fff',
    fontFamily: 'JosefinSans_400Regular',
    fontSize: 12,
  },
})