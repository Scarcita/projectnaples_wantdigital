import React from "react";
import { View,Text, StyleSheet, Image, TouchableOpacity, SafeAreaView, FlatList, TextInput, ScrollView } from "react-native";
import ListEvents from '../../../components/Screen/calendarScreen/listaEvents'
import {
  useFonts,
  JosefinSans_100Thin,
  JosefinSans_200ExtraLight,
  JosefinSans_300Light,
  JosefinSans_400Regular,
  JosefinSans_500Medium,
  JosefinSans_600SemiBold,
  JosefinSans_700Bold,
  JosefinSans_100Thin_Italic,
  JosefinSans_200ExtraLight_Italic,
  JosefinSans_300Light_Italic,
  JosefinSans_400Regular_Italic,
  JosefinSans_500Medium_Italic,
  JosefinSans_600SemiBold_Italic,
  JosefinSans_700Bold_Italic,
} from '@expo-google-fonts/josefin-sans';

//// IMAGENES////////

import ImgEvent from '../../../assets/Calendar/postEventSave.svg'
import ImgButtonSave from '../../../assets/Calendar/buttonSave.svg'
import DateCalendar from "./dateCalendar";

const CalendarScreen = ({navigation}) => {
  let [fontsLoaded] = useFonts({
    JosefinSans_100Thin,
    JosefinSans_200ExtraLight,
    JosefinSans_300Light,
    JosefinSans_400Regular,
    JosefinSans_500Medium,
    JosefinSans_600SemiBold,
    JosefinSans_700Bold,
    JosefinSans_100Thin_Italic,
    JosefinSans_200ExtraLight_Italic,
    JosefinSans_300Light_Italic,
    JosefinSans_400Regular_Italic,
    JosefinSans_500Medium_Italic,
    JosefinSans_600SemiBold_Italic,
    JosefinSans_700Bold_Italic,
    });


  return ( 
    <ScrollView>
      <View style={styles.container}>
      <View>
        <Text style={styles.title}>Events calendar</Text>
      </View>
      <View>
        <TextInput 
          style={styles.input}
          placeholder='Search event'
        ></TextInput>
      </View>
      <View>
        <DateCalendar/>
      </View>
      <View>
        <ListEvents/>
      </View>
    </View>
    </ScrollView>

  )
}

export default CalendarScreen;

const styles = StyleSheet.create ({
  container: {
    flex: 1,
    paddingTop: 10,
    paddingLeft: 35,
    paddingRight:35,
    backgroundColor: '#fff'
  },
  title:{
    color: '#000000',
    fontSize: 17,
    fontFamily: 'JosefinSans_700Bold',
    marginTop: 30
  },
  input: {
    backgroundColor: '#F5F5F5',
    width: 324,
    height: 42,
    borderRadius: 17,
    alignSelf: 'center',
    marginTop: 15,
    paddingLeft:15,
    fontSize: 13,
    fontFamily: 'JosefinSans_300Light'
  },
})