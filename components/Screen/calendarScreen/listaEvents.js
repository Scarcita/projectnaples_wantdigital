import React, {useState, useEffect} from "react";
import { View,Text, StyleSheet, Image, TouchableOpacity, SafeAreaView, FlatList, TextInput } from "react-native";


//// IMAGENES////////
import ImgEvent from '../../../assets/Calendar/postEventSave'
import ImgButtonSave from '../../../assets/Calendar/buttonSave.svg'

const ListEvents = ({navigation}) => {

  const [isLoading, setLoading] = useState(true);
  const [data, setData]= useState([]);

  const getUsuarios = async () => {
    try{
      const response = await
      fetch("https://reqres.in/api/users?page=2");
      const json = await response.json();
      console.log(json);
      setData(json.data);
    } catch (error){
      console.error(error);
    } finally {
      setLoading(false);
    }
  }

  useEffect(() => {
    getUsuarios();
  }, []);

  return ( 
    <FlatList
      data = {data}
      keyExtractor= {({id}) => id}
      renderItem={({item})=> (
        <View  style={[styles.flex1, {
          flexDirection: "row", marginTop: 25
          }]}>
            <View style={{ flex: 2,}} >
              <ImgEvent style={styles.borderImg}/>
            </View>
            <View style={{ flex: 3, alignSelf: 'center',}} >
              <View style={styles.buttonNotificacion}>
                <Text style={styles.textNotification}>Business After 5</Text>
              </View>
              <Text style={styles.name}>{item.email}</Text>
              <Text style={styles.timeEvent}>10:00 AM to 05:00 PM</Text>
              <Text style={styles.nameLocation}>Rookery Bay</Text>
            </View>
            <View style={{ flex: 1, justifyContent:'center', alignItems:'center',}} >
              <ImgButtonSave/>   
            </View>
        </View>
      )} >       
  </FlatList>

  )
}

export default ListEvents;

const styles = StyleSheet.create ({
  flex1: {
    flex: 1,
  },
 
  borderImg: {
    width: 88,
    height: 78,
    borderRadius: 100,
  },
  buttonNotificacion: {
    width: 78,
    height: 14,
    backgroundColor: '#FDB827',
    borderRadius: 7,
    justifyContent:'center',
    alignItems:'center',
  },
  textNotification:{
    fontSize: 8,
    color: '#fff',
    alignSelf: 'center',
    alignSelf: 'center', 
    alignItems: 'center',
    fontFamily: 'JosefinSans_500Medium'
  },
  name: {
    fontSize: 14,
    fontFamily: 'JosefinSans_700Bold',
    color: '#000'
  },
  timeEvent: {
    fontSize: 13,
    fontFamily: 'JosefinSans_500Medium',
    color: '#747474'

  },
  timeEvent: {
    fontSize: 13,
    fontFamily: 'JosefinSans_500Medium',
    color: '#747474'
  },
  nameLocation: {
    fontSize: 13,
    fontFamily: 'JosefinSans_500Medium',
    color: '#747474'

  }
})