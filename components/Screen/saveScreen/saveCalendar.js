import React, {useState, useEffect} from "react";
import { View,Text, StyleSheet, Image, TouchableOpacity, SafeAreaView, FlatList, TextInput } from "react-native";


////IMAGENES//////
import ImgAjustes from '../../../assets/Calendar/ajustes.svg'
import ImgButtonSave from '../../../assets/Calendar/savedEvents.svg'
import ImgEvent from '../../../assets/Calendar/postEventSave'

const SaveCalendar = ({navigation}) => {
  const [isLoading, setLoading] = useState(true);
  const [data, setData]= useState([]);

  const getUsuarios = async () => {
    try{
      const response = await
      fetch("https://reqres.in/api/users?page=2");
      const json = await response.json();
      console.log(json);
      setData(json.data);
    } catch (error){
      console.error(error);
    } finally {
      setLoading(false);
    }
  }

  useEffect(() => {
    getUsuarios();
  }, []);


  return ( 
        <View style={styles.container}>        
          <View style= {styles.header}>
            <View>
                <Text style={styles.locationName}>
                    Saved Events
                </Text>
              </View>
              <View>
              <ImgAjustes
                style={styles.ImgLocation}
                />
              </View>
        </View>
        <FlatList
        //horizontal = {true}
          data = {data}
          keyExtractor= {({id}) => id}
          renderItem={({item})=> (
            <View style={styles.flexEvent}>
                 <View style={styles.descriptionEvent}>
                    <View style={styles.cardDateEvent}>
                      <View style={styles.cardContentEvent}>
                          <Text style={styles.nameDayEvent}>Mon</Text>
                          <Text style={styles.dayDateEvent}>19</Text>
                          <Text style={styles.nameMonthEvent}>Dec</Text>
                      </View>
                    </View>
                    <View style={styles.informationEvent}>
                      <View style={styles.buttonNotificacion}>
                        <Text style={styles.textNotification}>Business After 5</Text>
                      </View>
                        <Text style={styles.nameEvent}>Holiday Crafts for Kids</Text>
                        <Text style={styles.dateEvent}>10:00 AM to 05:00 PM</Text>
                        <Text style={styles.locationEvent}>Rookery Bay</Text>
                    </View>
                </View>
                <View style={{alignSelf: 'center'}}>
                    <TouchableOpacity
                        onPress={() => navigation.navigate("DeshtokTwo")}
                        style={styles.buttonSave}
                    >
                    <View style={{ flex: 1, alignSelf: 'center', alignItems: 'center', justifyContent:'center',
                      alignItems:'center'}} >
                      <ImgButtonSave />   
                    </View>
                    </TouchableOpacity>
                </View>
            </View>
          )} >       
        </FlatList>
      </View>
      
    
  )
    
}

export default SaveCalendar;

const styles = StyleSheet.create ({
  container: {
    flex: 1,
    paddingTop: 10,
    paddingLeft: 35,
    paddingRight: 35,
    backgroundColor: '#fff' 
  },
  header: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 30,
  },
  locationName: {
    color: '#000',
    fontFamily: 'JosefinSans_700Bold',
    fontSize: 24,
    marginLeft: 10,
    alignSelf: 'center',
  },
ImgLocation: {
    textAlign: "center",
    alignSelf: 'center',
    alignItems: 'center'
  },

  flexEvent: {
    flexDirection: 'row',
    justifyContent: 'space-between',

  },
  descriptionEvent:{
    flexDirection: 'row',
    marginTop: 30

  },
  cardDateEvent: {
    width: 77, 
    height: 77, 
    backgroundColor: "rgba(0, 181, 195, 0.87)",
    borderRadius: 17,
    justifyContent:'center',
    alignItems:'center',
    textAlign: 'center',
  },

  cardContentEvent: {
    justifyContent:'center',
    alignItems:'center',
  },
  nameDayEvent: {
    color: '#fff',
    fontFamily: 'JosefinSans_400Regular',
    fontSize: 12,
  },
  dayDateEvent: {
    color: '#fff',
    fontFamily: 'JosefinSans_700Bold',
    fontSize: 24,
  },
  nameMonthEvent: {
    color: '#fff',
    fontFamily: 'JosefinSans_400Regular',
    fontSize: 12,
  },
  informationEvent: {
    marginLeft: 15,
    alignSelf: 'center'
  },
  buttonNotificacion: {
    width: 78,
    height: 14,
    backgroundColor: '#FDB827',
    borderRadius: 7,
    justifyContent:'center',
    alignItems:'center',
  },
  textNotification:{
    fontSize: 8,
    color: '#fff',
    alignSelf: 'center',
    alignSelf: 'center', 
    alignItems: 'center',
    fontFamily: 'JosefinSans_500Medium',
  },
  nameEvent: {
    fontSize: 14,
    color: '#000000',
    fontFamily: 'JosefinSans_700Bold'
  },
  dateEvent: {
    fontSize: 12,
    color: '#747474',
    fontFamily: 'JosefinSans_500Medium',

  },
  locationEvent: {
    fontSize: 12,
    color: '#747474',
    fontFamily: 'JosefinSans_500Medium',

  },
  buttonSave: {
    backgroundColor: "#FF0000",
    width: 38,
    height: 38,
    alignSelf: 'center',
    alignSelf: 'center',
    borderRadius: 10,

  },



 
});
