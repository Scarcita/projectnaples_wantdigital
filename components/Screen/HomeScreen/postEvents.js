import React, {useState, useEffect} from "react";
import { View,Text, StyleSheet, Image, TouchableOpacity, SafeAreaView, FlatList, TextInput } from "react-native";
import {
  useFonts,
  JosefinSans_100Thin,
  JosefinSans_200ExtraLight,
  JosefinSans_300Light,
  JosefinSans_400Regular,
  JosefinSans_500Medium,
  JosefinSans_600SemiBold,
  JosefinSans_700Bold,
  JosefinSans_100Thin_Italic,
  JosefinSans_200ExtraLight_Italic,
  JosefinSans_300Light_Italic,
  JosefinSans_400Regular_Italic,
  JosefinSans_500Medium_Italic,
  JosefinSans_600SemiBold_Italic,
  JosefinSans_700Bold_Italic,
} from '@expo-google-fonts/josefin-sans';

////IMAGENES///////////
import ImgCardPost from '../../../assets/Calendar/postDetail.svg'
import ImgNotification from '../../../assets/Calendar/notification.svg'

const PostEvents = () => {
  let [fontsLoaded] = useFonts({
    JosefinSans_100Thin,
    JosefinSans_200ExtraLight,
    JosefinSans_300Light,
    JosefinSans_400Regular,
    JosefinSans_500Medium,
    JosefinSans_600SemiBold,
    JosefinSans_700Bold,
    JosefinSans_100Thin_Italic,
    JosefinSans_200ExtraLight_Italic,
    JosefinSans_300Light_Italic,
    JosefinSans_400Regular_Italic,
    JosefinSans_500Medium_Italic,
    JosefinSans_600SemiBold_Italic,
    JosefinSans_700Bold_Italic,
    });


  const [isLoading, setLoading] = useState(true);
  const [data, setData]= useState([]);

  const getUsuarios = async () => {
    try{
      const response = await
      fetch("https://reqres.in/api/users?page=2");
      const json = await response.json();
      console.log(json);
      setData(json.data);
    } catch (error){
      console.error(error);
    } finally {
      setLoading(false);
    }
  }

  useEffect(() => {
    getUsuarios();
  }, []);


    return(
      <FlatList
      horizontal={true}
      data = {data}
      keyExtractor= {({id}) => id}
      renderItem={({item})=> (
        <View style={{marginRight: 20}}>
            <ImgCardPost style={styles.ImgCard}></ImgCardPost>
            <View style={styles.flexObject}>
              <View style={styles.cardDate}>
                <View style={styles.cardContent}>
                  <Text style={styles.nameDay}>Mon</Text>
                  <Text style={styles.dayDate}>19</Text>
                  <Text style={styles.nameMonth}>Dec</Text>
                </View>
              </View>
            <View>
            <ImgNotification style={styles.notification}></ImgNotification>
            </View>
            </View>
        </View>
        )} >       
    </FlatList>
    )
}

const styles = StyleSheet.create ({
    container: {
      flex: 1,  
    },
    ImgCard: {
      alignSelf: 'center',
    },
    flexObject: {
      flexDirection: 'row',
      justifyContent: 'space-between',
      position: 'absolute'
    },
    cardDate: {
      width: 38, 
      height: 38, 
      backgroundColor: '#00B5C3', 
      borderRadius: 5,
      marginLeft: 5,
      marginTop: 5,
      justifyContent:'center',
      alignItems:'center',
    },
  
    cardContent: {
      justifyContent:'center',
      alignItems:'center',
      
    },
    nameDay: {
      color: '#fff',
      fontFamily: 'JosefinSans_400Regular',
      fontSize: 8,
    },
    dayDate: {
      color: '#fff',
      fontFamily: 'JosefinSans_700Bold',
      fontSize: 12,
    },
    nameMonth: {
      color: '#fff',
      fontFamily: 'JosefinSans_400Regular',
      fontSize: 8,
    },
    notification:{
      marginLeft: 165,
      marginTop: 25
  
    },
   
  });

export default PostEvents;