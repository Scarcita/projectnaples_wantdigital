import React, { useState, useEffect } from "react";
import { View, Text, StyleSheet, Image, TouchableOpacity, SafeAreaView, FlatList, TextInput } from "react-native";
import { useNavigation } from "@react-navigation/native";
import {
  useFonts,
  JosefinSans_100Thin,
  JosefinSans_200ExtraLight,
  JosefinSans_300Light,
  JosefinSans_400Regular,
  JosefinSans_500Medium,
  JosefinSans_600SemiBold,
  JosefinSans_700Bold,
  JosefinSans_100Thin_Italic,
  JosefinSans_200ExtraLight_Italic,
  JosefinSans_300Light_Italic,
  JosefinSans_400Regular_Italic,
  JosefinSans_500Medium_Italic,
  JosefinSans_600SemiBold_Italic,
  JosefinSans_700Bold_Italic,
} from '@expo-google-fonts/josefin-sans';

const UpcomingEvents = () => {
  let [fontsLoaded] = useFonts({
    JosefinSans_100Thin,
    JosefinSans_200ExtraLight,
    JosefinSans_300Light,
    JosefinSans_400Regular,
    JosefinSans_500Medium,
    JosefinSans_600SemiBold,
    JosefinSans_700Bold,
    JosefinSans_100Thin_Italic,
    JosefinSans_200ExtraLight_Italic,
    JosefinSans_300Light_Italic,
    JosefinSans_400Regular_Italic,
    JosefinSans_500Medium_Italic,
    JosefinSans_600SemiBold_Italic,
    JosefinSans_700Bold_Italic,
    });


  const navigation = useNavigation();
  const [isLoading, setLoading] = useState(true);
  const [data, setData] = useState([]);

  const getUsuarios = async () => {
    try {
      const response = await
        fetch("https://reqres.in/api/users?page=2");
      const json = await response.json();
      console.log(json);
      setData(json.data);
    } catch (error) {
      console.error(error);
    } finally {
      setLoading(false);
    }
  }

  useEffect(() => {
    getUsuarios();
  }, []);

  return (
    <FlatList
      data={data}
      keyExtractor={({ id }) => id}
      renderItem={({ item }) => (
        <View>
          <View style={styles.flexEvent}>
            <View style={styles.descriptionEvent}>
              <View style={styles.cardDateEvent}>
                <View style={styles.cardContentEvent}>
                  <Text style={styles.nameDayEvent}>Mon</Text>
                  <Text style={styles.dayDateEvent}>19</Text>
                  <Text style={styles.nameMonthEvent}>Dec</Text>
                </View>
              </View>
              <View style={styles.informationEvent}>
                <Text style={styles.nameEvent}>Holiday Crafts for Kids</Text>
                <Text style={styles.dateEvent}>10:00 AM to 05:00 PM</Text>
                <Text style={styles.locationEvent}>Rookery Bay</Text>
              </View>
            </View>
            <View style={{ alignSelf: 'center' }}>
              <TouchableOpacity
                onPress={() => (navigation.navigate('DetailEvets'))}
                style={styles.button}
              >
                <Text
                  style={styles.textButton}
                >
                  + Inf
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      )} >
    </FlatList>

  )
}

export default UpcomingEvents;

const styles = StyleSheet.create({
  flexEvent: {
    flexDirection: 'row',
    justifyContent: 'space-between',

  },
  descriptionEvent: {
    flexDirection: 'row',
    marginTop: 30

  },
  cardDateEvent: {
    width: 68,
    height: 68,
    backgroundColor: '#00B5C3',
    borderRadius: 9,
    justifyContent: 'center',
    alignItems: 'center',
  },

  cardContentEvent: {
    justifyContent:'center',
    alignItems:'center',
  },
  nameDayEvent: {
    color: '#fff',
    fontFamily: 'JosefinSans_400Regular',
    fontSize: 12,
  },
  dayDateEvent: {
    color: '#fff',
    fontFamily: 'JosefinSans_700Bold',
    fontSize: 24,
  },
  nameMonthEvent: {
    color: '#fff',
    fontFamily: 'JosefinSans_400Regular',
    fontSize: 12,
  },
  informationEvent: {
    marginLeft: 15,
    alignSelf: 'center'
  },
  nameEvent: {
    fontSize: 14,
    color: '#000000',
    fontFamily: 'JosefinSans_700Bold',
  },
  dateEvent: {
    fontSize: 12,
    color: '#747474',
    fontFamily: 'JosefinSans_500Medium',
  },
  locationEvent: {
    fontSize: 12,
    color: '#747474',
    fontFamily: 'JosefinSans_500Medium',

  },
  button: {
    backgroundColor: "#D9D9D9",
    marginTop: 20,
    width: 78,
    height: 26,
    alignSelf: 'center',
    borderRadius: 7,
    justifyContent:'center',
    alignItems:'center',

  },
  textButton: {
    textAlign: 'center',
    fontSize: 14,
    fontFamily: 'JosefinSans_700Bold',
    color: '#FFFFFF',

  }

});