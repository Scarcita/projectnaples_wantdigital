import React from "react";
import { View,Text, StyleSheet, Image, TouchableOpacity, SafeAreaView, FlatList, TextInput, ScrollView } from "react-native";
import {
  useFonts,
  JosefinSans_100Thin,
  JosefinSans_200ExtraLight,
  JosefinSans_300Light,
  JosefinSans_400Regular,
  JosefinSans_500Medium,
  JosefinSans_600SemiBold,
  JosefinSans_700Bold,
  JosefinSans_100Thin_Italic,
  JosefinSans_200ExtraLight_Italic,
  JosefinSans_300Light_Italic,
  JosefinSans_400Regular_Italic,
  JosefinSans_500Medium_Italic,
  JosefinSans_600SemiBold_Italic,
  JosefinSans_700Bold_Italic,
} from '@expo-google-fonts/josefin-sans';

////IMAGENES//////

import ImgLocation from '../../../assets/Calendar/location.svg'
import FeatureEvents from "../../../components/Screen/HomeScreen/featuredEvents";
import UpcomingEvents from "../../../components/Screen/HomeScreen/upcomingEvents";
import PostEvents from "../../../components/Screen/HomeScreen/postEvents";
import AppLoading from 'expo-app-loading';

const CalendarEvents = ({navigation}) => {
  
  let [fontsLoaded] = useFonts({
    JosefinSans_100Thin,
    JosefinSans_200ExtraLight,
    JosefinSans_300Light,
    JosefinSans_400Regular,
    JosefinSans_500Medium,
    JosefinSans_600SemiBold,
    JosefinSans_700Bold,
    JosefinSans_100Thin_Italic,
    JosefinSans_200ExtraLight_Italic,
    JosefinSans_300Light_Italic,
    JosefinSans_400Regular_Italic,
    JosefinSans_500Medium_Italic,
    JosefinSans_600SemiBold_Italic,
    JosefinSans_700Bold_Italic,
    });

  return ( 
    <ScrollView>
      <View style={styles.container}>
          <Text style={styles.location}>
            Current location
          </Text>
          <View style= {styles.header}>
            <View style= {styles.headerLeft}>
                <ImgLocation
                style={styles.ImgLocation}
                />
                <Text style={styles.locationName}>
                    Naples FL
                </Text>
            </View>
            <View >
                <Text style= {styles.headerRigh}></Text>

            </View>

          </View>

          <View>
            <TextInput 
            style={styles.input}
            placeholder='Search event'
            ></TextInput>
          </View>
          <View>
            <View style= {styles.twoSeccion}>
              <View>
                  <Text style={styles.titleEvent}>
                  Featured Events
                  </Text>
              </View>
              <View >
                <TouchableOpacity
                    onPress={() => (navigation.navigate('two'))}
                >
                  <Text style= {styles.view}>
                      View all
                  </Text>
                </TouchableOpacity>
                  
              </View>
            </View>
            <FeatureEvents/>
          </View>
          <View>
            <View style={styles.threeSeccion}>
              <View>
                  <Text style={styles.titleUpcomingEvent}>
                    Upcoming Events
                  </Text>
              </View>
              <View >
                <TouchableOpacity
                  onPress={() => (navigation.navigate('two'))}
              >
                  <Text style= {styles.UpcomingView}>
                      View all
                  </Text>
                </TouchableOpacity>
              </View>
            </View>
            <UpcomingEvents/>
          </View>
          <View>
            <View style={styles.threeSeccion}>
              <View>
                  <Text style={styles.titleUpcomingEvent}>
                    Past Events
                  </Text>
              </View>
              <View >
                <TouchableOpacity
                    onPress={() => (navigation.navigate('two'))}
                >
                    <Text style= {styles.UpcomingView}>
                      View all
                  </Text>
                </TouchableOpacity>
              </View>
            </View>
            <PostEvents/>
          </View>
        </View>
    </ScrollView>
  )

    
}

export default CalendarEvents;

const styles = StyleSheet.create ({
  container: {
    flex: 1,
    paddingTop: 10,
    paddingLeft: 35,
    paddingRight:35,
    backgroundColor: '#fff' 
  },
  location: {
    color: '#00B5C3',
    fontFamily: 'JosefinSans_300Light',
    fontSize: 13,
    marginTop: 35,

  },
  header: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 8,
  },
  headerLeft: {
    flexDirection: 'row',
  },
  locationName: {
    color: '#00B5C3',
    fontSize: 24,
    marginLeft: 10,
    alignSelf: 'center',
    fontFamily: 'JosefinSans_700Bold',
  },
ImgLocation: {
    textAlign: "center",
    alignSelf: 'center',
    width: '100%',
    height: '100%'
  },
  headerRigh: {
    backgroundColor: "#00B5C3",
    width: 54,
    height: 54,
    borderRadius: 100,
    alignSelf: 'center',
  },
  input: {
    backgroundColor: '#F5F5F5',
    width: 324,
    height: 42,
    borderRadius: 17,
    alignSelf: 'center',
    marginTop: 15,
    paddingLeft:15,
    fontFamily: 'JosefinSans_400Regular'
  },
  twoSeccion: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 20,
  },
  titleEvent: {
    color: '#000000',
    fontSize: 17,
    fontFamily: 'JosefinSans_700Bold',
  },
  view: {
    color: '#747474',
    fontSize: 14,
    fontFamily: 'JosefinSans_300Light',
  },
  threeSeccion: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 30,
    marginBottom: 25
  },
  titleUpcomingEvent: {
    color: '#000000',
    fontSize: 17,
    fontFamily: 'JosefinSans_700Bold',
  },
  UpcomingView: {
    color: '#747474',
    fontSize: 14,
    fontFamily: 'JosefinSans_300Light',
    alignSelf: 'center',
  },
 
});
