import React from "react";
import { View,Text, StyleSheet, Image, TouchableOpacity, SafeAreaView, FlatList, TextInput, ScrollView } from "react-native";
import { LinearGradient } from 'expo-linear-gradient';
import MapView from "react-native-maps";

////IMAGENES//////
import ImgBack from '../../../../assets/Calendar/goBack.svg'
import ImgPostDetail from '../../../../assets/Calendar/postDetail.svg'
import ImgLocationDetail from '../../../../assets/Calendar/locationDetail.svg'
import ImgButtonSave from '../../../../assets/Calendar/buttonSaveDetail.svg'
import ImgTicket from '../../../../assets/Calendar/ticket.svg'
import ListaPeople from '../../../../components/Screen/HomeScreen/detailEvent/listUsuarios'

const DetailEvets = ({navigation}) => {
  return ( 
        <ScrollView>
          <View style={styles.container}>
            <LinearGradient
              colors={['#00B5C3', '#077477']}
              style={styles.portadaPost}>
                <View style={styles.header}>
                    <ImgBack onPress={() => navigation.goBack()}></ImgBack>
                    <View style={styles.buttonNotificacion}>
                      <Text style={styles.textNotification}>Business After 5</Text>
                    </View>
                </View>
                <View style={[{
                    flexDirection: "row", justifyContent: 'space-between', marginTop: 30
                  }]}>
                  <View style={{flex: 1, marginLeft: 20, marginRight: 2}}>
                    <View style={styles.cuadroText}>
                      <Text style={styles.titleAnuncio}>Reindeer Run! 5K</Text>
                    </View>
                    <View style={styles.dateTime}>
                      <View style={styles.backgroundDate}>
                        <View style={styles.dateAll}>
                          <Text style={styles.nameDay}>Mon</Text>
                          <Text style={styles.dayDate}>19</Text>
                          <Text style={styles.nameMonth}>Dec</Text>
                        </View>
                      </View>
                      <View style={styles.detailDate}>
                        <Text style={styles.from}>From</Text>
                        <Text style={styles.dateStart}>6:00 PM</Text>
                        <Text style={styles.to}>To</Text>
                        <Text style={styles.dateEnd}>10:00 PM</Text>
                      </View>
                    </View>
                    <View style={styles.detailLocation}>
                      <View style={{flexDirection: 'row'}}>
                        <ImgLocationDetail/>
                        <Text style={styles.descriptionLocation}>300 Tower RoadNaples, FL 34113</Text>
                      </View>
                    </View>
                  </View>
                  
                  <View style={{ flex: 1, marginRight: 20, marginLeft: 2, height: 225, width: 250, justifyContent: 'flex-end', alignItems: 'flex-end'}} >
                    <ImgPostDetail style={styles.imgPost}/>
                  </View>
                </View>
            </LinearGradient>
            <View style={[{
                flexDirection: "row", marginLeft: 20, marginRight: 20, marginTop: 25
              }]}>
              <View style={{ flex: 2,}} >
                <ListaPeople style={{justifyContent:'center', alignItems:'center'}}/>
              </View>
              <View style={{ flex: 3, justifyContent:'center', alignItems:'center'}} >
                <Text style={styles.peopleActivos}>Over 40 people joined this event</Text>
              </View>
              <View style={{ flex: 1, flexDirection: 'row'}} >
                <ImgTicket style={styles.ticket}/>
                <Text style={styles.monto}>450$</Text>
              </View>
            </View>
            <View  style={[{
                   marginTop: 20, marginRight: 20,marginLeft: 20,
                  }]}>
              <View >
                <Text style={styles.descriptionEvent}>
                  Anyone who has been to a Girls on the Run 5K knows that it is unlike any other running event. It is a celebration of and for the girls who are completing an 10 week season of Girls on the Run programming, coaches, and the community! This y... View more
                </Text>
                
              </View>
            </View>
            <View style={[{
                flexDirection: "row", marginLeft: 20, marginRight: 20, marginTop: 25
              }]}>
              <View style={{ flex: 2, justifyContent:'center', alignItems:'center'}} >
                <Text style={styles.startEvent}>Event starts in</Text>
              </View>
              <View style={{ flex: 3,}} >
                <View style={{flexDirection: "row", justifyContent: 'space-evenly'}}>
                  <View style={styles.borderDate}>
                    <Text style={styles.day}>04</Text>
                    <Text style={styles.dayLiteral}>Days</Text>
                  </View>
                  <View style={styles.borderDate}>
                    <Text style={styles.day}>18</Text>
                    <Text style={styles.dayLiteral}>Hours</Text>
                  </View>
                  <View style={styles.borderDate}>
                    <Text style={styles.day}>20</Text>
                    <Text style={styles.dayLiteral}>Seconds</Text>
                  </View>
                </View>
              </View>
            </View>
            <MapView
              style={styles.mapa}
              initialRegion={{
              latitude: 23.752538,
              longitude: -99.141926,
              latitudeDelta: 0.01,
              longitudeDelta: 0.01
              }}>
            </MapView>
            <View style={{flexDirection: 'row', justifyContent: 'space-between', marginLeft: 20, marginRight: 20, marginTop: 30, marginBottom: 20}}>
              <View style={{alignSelf: 'center'}}>
                <TouchableOpacity
                    onPress={() => navigation.navigate("DeshtokTwo")}
                    style={styles.buttonSave}
                >
                <View style={{ flex: 1, alignSelf: 'center', alignItems: 'center', justifyContent:'center'}} >
                  <ImgButtonSave />   
                </View>
                </TouchableOpacity>
              </View>
              <TouchableOpacity
                onPress={() => navigation.navigate("DeshtokTwo")}
                    style={styles.button}
                >
                <Text
                    style={styles.textButton}
                >
                    Join now
                </Text>
              </TouchableOpacity>
            </View>
        </View>
      </ScrollView>
  )
    
}

export default DetailEvets;

const styles = StyleSheet.create ({
  container: {
    flex: 1,
    backgroundColor: '#fff'
  },
  portadaPost: {
    width: '100%',
    height: 320,
    borderBottomRightRadius: 17,
    borderBottomLeftRadius: 17,
    //backgroundColor: '#00B5C3, #077477'
  },
  header: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingTop: 15,
    paddingLeft: 20,
    paddingRight:20
 
  },
  buttonNotificacion: {
    width: 105,
    height: 20,
    backgroundColor: '#FDB827',
    borderRadius: 10,
    justifyContent: 'center',
    alignItems: 'center',
  },
  textNotification:{
    textAlign: 'center',
    color: '#fff',
    alignSelf: 'center'
  },
  flex: {
    flex: 1,
  },
  cuadroText: {
    width: 170,
    height: 90,
    //backgroundColor: 'blue',
  },

  titleAnuncio: {
    fontSize: 30,
    fontFamily: 'JosefinSans_700Bold',
    color: '#fff'

  },
  dateTime:{
    flexDirection: 'row',
    //backgroundColor: 'green',
    width: 170,
    height: 90,
  },

  backgroundDate:{
    width: 65,
    height:65,
    backgroundColor: '#fff',
    borderRadius: 9,
    alignSelf: 'center',
    justifyContent:'center',
    alignItems:'center',
  },
  dateAll:{
    justifyContent:'center',
    alignItems:'center',
  },
  nameDay: {
    color: '#FF0000',
    fontFamily: 'JosefinSans_400Regular',
    fontSize: 12,
  },
  dayDate: {
    color: '#000',
    fontFamily: 'JosefinSans_700Bold',
    fontSize: 24,
  },
  nameMonth: {
    color: '#000',
    fontFamily: 'JosefinSans_400Regular',
    fontSize: 12,
  },
  detailDate: {
    marginLeft: 10,
    alignSelf: 'center'
  },
  from: {
    fontSize: 7,
    color: '#fff',
    fontFamily: 'JosefinSans_300Light',
  },
  dateStart:{
    fontSize: 20,
    fontFamily: 'JosefinSans_700Bold',
    color: '#fff'
  },
  to: {
    fontSize: 7,
    color: '#fff',
    fontFamily: 'JosefinSans_300Light',
  },
  dateEnd:{
    fontSize: 20,
    fontFamily: 'JosefinSans_700Bold',
    color: '#fff'
  },
  detailLocation: {
    width: 170,
    height: 50,
    //backgroundColor: 'red',
    
  },
  descriptionLocation: {
    fontSize: 15,
    fontFamily: 'JosefinSans_400Regular',
    color: '#fff',
    marginLeft: 10,
  },

  imgPost:{
    width: 250,
    height: 225,
    borderRadius: 40,
  },

  peopleActivos: {
    fontSize: 12,
    fontFamily: 'JosefinSans_700Bold',
    color: '#000',
    marginLeft: 5,
    textAlign: 'center',
    alignSelf: 'center',
    justifyContent:'center',
    alignItems:'center',
  },
  ticket:{
    justifyContent:'center',
    alignItems:'center',
    alignSelf: 'center',
  },
  monto: {
    color: '#00B5C3',
    fontSize: 16,
    fontFamily: 'JosefinSans_300Light',
    justifyContent:'center',
    alignItems:'center',
    alignSelf: 'center',

  },
  descriptionEvent:{
    color: '##747474',
    fontSize: 12,
    fontFamily: 'JosefinSans_300Light',
    justifyContent:'center',
    alignItems:'center',
    alignSelf: 'center',

  },
  startEvent: {
    fontSize: 12,
    fontFamily: 'JosefinSans_300Light',
    color: '#000',
    justifyContent:'center',
    alignItems:'center',
    alignSelf: 'center',

  },
  borderDate: {
    width: 48,
    height: 48,
    borderWidth: 4,
    borderColor: '#FDB827',
    borderRadius: 100,
    justifyContent:'center',
    alignItems:'center',
  },
  day: {
    fontSize: 14,
    fontFamily: 'JosefinSans_700Bold',
    textAlign: 'center',
  },
  dayLiteral: {
    fontSize: 8,
    fontFamily: 'JosefinSans_700Bold',
    textAlign: 'center',
  },
  mapa: {
    height: 160,
    marginLeft: 20,
    marginRight: 20,
    marginTop: 30
  },
  buttonSave: {
    width: 58,
    height: 58,
    borderWidth: 2,
    borderColor: '#D9D9D9',
    borderRadius: 9,
    justifyContent:'center',
    alignItems:'center',
  },
  button: {
    backgroundColor: "#00B5C3",
    width: 258,
    height: 62,
    justifyContent:'center',
    alignItems:'center',
    borderRadius: 17,

  },
  textButton: {
    textAlign: 'center',
    fontSize: 24,
    color: '#FFFFFF',
    justifyContent:'center',
    alignItems:'center',
    fontFamily: 'JosefinSans_300Light',
    //fontFamily: 'Prompt_600SemiBold',
  }


 
});
