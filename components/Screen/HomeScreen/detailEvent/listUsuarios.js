import React, {useState, useEffect} from "react";
import { View,Text, StyleSheet, Image, TouchableOpacity, StatusBar, ActivityIndicator, FlatList } from "react-native";


const ListaPeople = () => {
  const [isLoading, setLoading] = useState(true);
  const [data, setData]= useState([]);

  const getUsuarios = async () => {
    try{
      const response = await
      fetch("https://reqres.in/api/users?page=2");
      const json = await response.json();
      console.log(json);
      setData(json.data);
    } catch (error){
      console.error(error);
    } finally {
      setLoading(false);
    }
  }

  useEffect(() => {
    getUsuarios();
  }, []);

  return (
    <View style={styles.container}>
      {isLoading ? <ActivityIndicator/> : (
        <FlatList
        horizontal = {true}
        data = {data}
        keyExtractor= {({id}) => id}
        renderItem={({item})=> (
          <View style = {{justifyContent:'center', alignItems:'center'}}>
            <Image style = {styles.image} source = {item.avatar}/>
          </View>
        )} ></FlatList>
        
      ) }
    </View>
  );
    
}

export default ListaPeople;

const styles = StyleSheet.create ({
  image: {
    width: 20,
    height: 20,
    border: 2,
    marginBottom: 15,
    borderRadius: 50,
    backgroundColor: '#D9D9D9',
    justifyContent:'center', 
    alignItems:'center'
    // border: '2px solid #FDFDFD',
    // borderRadius: '50%'
  }
});
