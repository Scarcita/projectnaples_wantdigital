import React, {useState, useEffect} from "react";
import { View,Text, StyleSheet, Image, TouchableOpacity, SafeAreaView, FlatList, TextInput } from "react-native";
import {
  useFonts,
  JosefinSans_100Thin,
  JosefinSans_200ExtraLight,
  JosefinSans_300Light,
  JosefinSans_400Regular,
  JosefinSans_500Medium,
  JosefinSans_600SemiBold,
  JosefinSans_700Bold,
  JosefinSans_100Thin_Italic,
  JosefinSans_200ExtraLight_Italic,
  JosefinSans_300Light_Italic,
  JosefinSans_400Regular_Italic,
  JosefinSans_500Medium_Italic,
  JosefinSans_600SemiBold_Italic,
  JosefinSans_700Bold_Italic,
} from '@expo-google-fonts/josefin-sans';

////IMAGENES///////////
import ImgCard from '../../../assets/Calendar/post.svg'
import ImgNotification from '../../../assets/Calendar/notification.svg'

const FeatureEvents = () => {
  let [fontsLoaded] = useFonts({
    JosefinSans_100Thin,
    JosefinSans_200ExtraLight,
    JosefinSans_300Light,
    JosefinSans_400Regular,
    JosefinSans_500Medium,
    JosefinSans_600SemiBold,
    JosefinSans_700Bold,
    JosefinSans_100Thin_Italic,
    JosefinSans_200ExtraLight_Italic,
    JosefinSans_300Light_Italic,
    JosefinSans_400Regular_Italic,
    JosefinSans_500Medium_Italic,
    JosefinSans_600SemiBold_Italic,
    JosefinSans_700Bold_Italic,
    });

  const [isLoading, setLoading] = useState(true);
  const [data, setData]= useState([]);

  const getUsuarios = async () => {
    try{
      const response = await
      fetch("https://reqres.in/api/users?page=2");
      const json = await response.json();
      console.log(json);
      setData(json.data);
    } catch (error){
      console.error(error);
    } finally {
      setLoading(false);
    }
  }

  useEffect(() => {
    getUsuarios();
  }, []);


    return(
      <FlatList
      horizontal={true}
      data = {data}
      keyExtractor= {({id}) => id}
      renderItem={({item})=> (
        <View style={{marginRight: 20, marginBottom: 10}}>
          <View style={styles.card}>
            <View>
              <ImgCard style={styles.ImgCard}></ImgCard>
              <View style={styles.flexObject}>
                <View style={styles.cardDate}>
                  <View style={styles.cardContent}>
                    <Text style={styles.nameDay}>Mon</Text>
                    <Text style={styles.dayDate}>19</Text>
                    <Text style={styles.nameMonth}>Dec</Text>
                  </View>
                </View>
                <View>
                <ImgNotification style={styles.notification}></ImgNotification>
                </View>
              </View>
            </View>
            <View style={styles.seccionText}>
              <Text style={styles.titleCard}>
                Greater Naples Chamber Jingle & Mingle
              </Text>
              <Text style={styles.descriptionCard}>
                Networking Event
              </Text>
            </View>
          </View>
        </View>
        )} >       
    </FlatList>
    )
}

const styles = StyleSheet.create ({
    container: {
      flex: 1,  
    },
    card: {
      width: 272,
      height: 320,
      backgroundColor: '#fff',
      alignSelf: 'center',
      marginTop: 22,
      borderBottomRightRadius: 17,
      borderBottomLeftRadius: 17,
      shadowColor: "#000",
      shadowOffset: {
        width: 0,
        height: 4,
      },
      shadowOpacity: 0.37,
      shadowRadius: 17,

      elevation: 5,
    },
    ImgCard: {
      alignSelf: 'center',
    },
    flexObject: {
      flexDirection: 'row',
      justifyContent: 'space-between',
      position: 'absolute'
    },
    cardDate: {
      width: 68, 
      height: 68, 
      backgroundColor: '#00B5C3', 
      borderRadius: 9,
      marginLeft: 5,
      marginTop: 5,
      justifyContent:'center',
      alignItems:'center',
    },
  
    cardContent: {
      justifyContent:'center',
      alignItems:'center',
      
    },
    nameDay: {
      color: '#fff',
      fontFamily: 'JosefinSans_400Regular',
      fontSize: 12,
    },
    dayDate: {
      color: '#fff',
      fontFamily: 'JosefinSans_700Bold',
      fontSize: 24,
    },
    nameMonth: {
      color: '#fff',
      fontFamily: 'JosefinSans_400Regular',
      fontSize: 12,
    },
    notification:{
      marginLeft: 165,
      marginTop: 25
  
    },
    seccionText:{
      marginLeft: 15,
      marginRight:15
    },
    titleCard: {
      fontSize: 14,
      fontFamily: 'JosefinSans_700Bold',
      marginTop: 15
    },
    descriptionCard: {
      fontSize: 14,
     fontFamily: 'JosefinSans_300Light',
      marginTop: 10
    },
   
  });

export default FeatureEvents;