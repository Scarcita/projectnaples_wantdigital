import React from "react";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import { StyleSheet, Text, View, Image, TouchableOpacity } from "react-native";
import CalendarEvents from "./components/Screen/HomeScreen/CalendarEvents";
import CalendarScreen from "./components/Screen/calendarScreen/calendar";
import SaveCalendar from "./components/Screen/saveScreen/saveCalendar";
import ProfileUser from './components/Screen/userScreen/user'
import { MaterialCommunityIcons } from '@expo/vector-icons';
import { Feather } from '@expo/vector-icons';
import { SafeAreaView, SafeAreaProvider } from "react-native-safe-area-context";
import Login from "./components/login/login";
import CreditCard from "./components/creditcard/creditcard";
import AnimationJson from "./components/animation";
import RegisterOne from "./components/login/DeshtokOne";
import Registertwo from "./components/login/DeshtokTwo";
import RegisterThrre from "./components/login/DeshtokThree";
import DetailEvets from "./components/Screen/HomeScreen/detailEvent/detailEvent";


const Tab = createBottomTabNavigator();
const Stack = createNativeStackNavigator()

const MyTabs = () => {
  return (
    <Tab.Navigator
    initialRouteName="Calendar"
      screenOptions={{
        headerShown: false,
        tabBarShowLabel: false,
        style: {
          position: 'absolute',
          elevation: 0,
          color: '#fff',
          backgroundColor: '#000000',
          with: '375pt',
          height: '100pt',
          
      }
    }}
    >
      <Tab.Screen 
        name="login" 
        component={Login}
        options={{
            tabBarIcon: ({ focused }) => (
            <View style={{ alignItems: 'center', justifyContent: 'center', top: 10}}
            >
            <MaterialCommunityIcons name="login-variant" size={24} color="black"style={{color: focused ? '#FDB827' : '#D7D7D7'}}/>
            </View>
            )   
        }}
      />
      <Tab.Screen 
        name="Calendar" 
        component={CalendarEvents}
        options={{
            tabBarIcon: ({ focused }) => (
            <View style={{ alignItems: 'center', justifyContent: 'center', top: 10}}
            >
            <MaterialCommunityIcons name="home-outline" size={26} color="black" style={{color: focused ? '#FDB827' : '#D7D7D7'}}/>
            </View>
            )   
        }}
      />
      <Tab.Screen 
        name="calendarEvent" 
        component={CalendarScreen}
        options={{
            tabBarIcon: ({ focused }) => (
            <View style={{ alignItems: 'center', justifyContent: 'center', top: 10}}
            >
            <MaterialCommunityIcons name="calendar-blank-outline" size={24} color="black" style={{color: focused ? '#FDB827' : '#D7D7D7'}}/>
            </View>
            )   
        }}
      />
      <Tab.Screen 
        name="save Event" 
        component={SaveCalendar}
        options={{
            tabBarIcon: ({ focused }) => (
            <View style={{ alignItems: 'center', justifyContent: 'center', top: 10}}
            >
            <MaterialCommunityIcons name="bookmark-outline" size={24} color="black" style={{color: focused ? '#FDB827' : '#D7D7D7'}}/>
            </View>
            )   
        }}
      />
       <Tab.Screen 
        name="profile" 
        component={ProfileUser}
        options={{
            tabBarIcon: ({ focused }) => (
            <View style={{ alignItems: 'center', justifyContent: 'center', top: 10}}
            >
            <Feather name="user" size={24} color="black" style={{color: focused ? '#FDB827' : '#D7D7D7'}} />
            </View>
            )   
        }}
      />
      
    </Tab.Navigator>
    );  
}

 function SignLog() {
  return(
    <Stack.Navigator
      initialRouteName="Login"
      screenOptions={{
        headerShown: false,
      }}
    >
      <Stack.Screen
        name="Login"
        component={Login}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="RegisterOne"
        component={RegisterOne}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="Registertwo"
        component={Registertwo}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="RegisterThrre"
        component={RegisterThrre}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="DetailEvets"
        component={DetailEvets}
        options={{ headerShown: false }}
      />
    </Stack.Navigator>
  )
}

function HomeStack() {
  return(
    <Stack.Navigator headerShown={false}>
      <Stack.Screen
        name="CreditCard"
        component={CreditCard}
        options={{ headerShown: false }}
      />
      <Stack.Screen 
        name="animacion" 
        component={AnimationJson}
        options={{ headerShown: false }}
      />
    </Stack.Navigator>
  )
}


function Navigation() {
  return(
    <Stack.Navigator screenOptions={{ headerShown: false }}>
      <Stack.Screen
        name="MyTabs"
        component={MyTabs}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="SignLog"
        component={SignLog}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="HomeStack"
        component={HomeStack}
        options={{ headerShown: false }}
      />
    </Stack.Navigator>
  )
}

export default () => {
  return (
    <SafeAreaProvider>
      <SafeAreaView style={{ flex: 1 , backgroundColor: 'red'}}>
          <Navigation/>
      </SafeAreaView>
    </SafeAreaProvider>
  );
};

